# Lazada test task

### Description
Using one of modern PHP frameworks (sf2, zf2, Laravel, Yii2) you should implement system
for API calls (no frontend part but you can add Swagger or similar tool to explore your api).
Your JSON-REST full api (not JSON-RPC) should provide CRUD access to two resources
without any security check:

- Post (with fields like: title, body and so on)
- Tags (with only name field) many-to-may relation with posts

Additional methods for API:
- select all posts by tag or tags
- count posts by tag or tags

After any post created system should send email to specified in configuration file Email.
 
After any post removed system should log information about it.

### Technologies used for this task
- Laravel Framework 5.2
- PHP 5.6
- MySQL
- Memcached
- Beanstalkd
- Swagger UI to visualize RESTFUL API (Frontend part)
- PHPUnit with Mockery
- Composer

### Installation

Everything is pre-configured in vagrant box. All you need is to have Vagrant and Virtualbox  installed on your system.

First of all, download vagrant box from this link https://1drv.ms/u/s!Ar9mxXAD5URskyWSvm8k4hMbogyH

This box is around 0.8GB and that's why i put on Microsft Onedrive service as bitbucket doesn't accept such huge file. 
After downloading this box (lazadatask.box), Copy this vagrant box file inside the same folder after cloning this repository.

On project root folder, run following commands.
```sh
$ vagrant box add lazadatask file://lazadatask.box
$ vagrant up
$ vagrant ssh
```

Vagrant SSH login/pass
```sh
user: vagrant
password: vagrant
```

The project is located in the root folder with name /lazadatask which will be mapped to /var/www.

In system hosts file, Add these lines.

```sh
192.168.33.10 lazadatask.dev
```

Open http://lazadatask.dev in browser. Frontend part is based on Swagger UI so All REST API methods are documented on frontend part using Swagger UI and can be tested directly.

Database and tables are already created in this vagrant box.

There is also phpmyadmin installed which can be accessed from url http://192.168.33.10/phpmyadmin/

```sh
Mysql username : root
Mysql password : root
```
The same user/pass combination works with phpmyadmin login. The database name for this project is **lazadatask**.

Emails from this application are sent using queue service **Beanstalkd**. **Supervisor** is preinstalled and configured. i.e Email will be sent when new post is added.
It needs to be enabled before testing email or make sure it is enabled.
```sh
sudo service supervisor start
```

Mailcatcher needs to be enabled in order to test emails.
```sh
mailcatcher --http-ip=0.0.0.0
```
Mailcacther can be accessed from http://192.168.33.10:1080

You can run tests for this task by using command as below
```sh
php vendor/bin/phpunit
```
