<?php

namespace App\Http\Controllers\Api;

use Response;
use Illuminate\Http\Request;
use App\Lazada\Repositories\Tag\TagRepositoryInterface;

/**
 * Class TagController
 * @package App\Http\Controllers
 */
class TagController extends Controller
{
    /**
     * @var TagRepositoryInterface
     */
    protected $tagRepository;

    /**
     * TagController constructor.
     *
     * @param TagRepositoryInterface $tagRepository
     */
    public function __construct(TagRepositoryInterface $tagRepository, TagRepositoryInterface $tagRepository)
    {
        parent::__construct();

        $this->tagRepository = $tagRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Get all tags
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $tags = $this->tagRepository->getAll();

        $this->response['data']['data'] = array('tags' => $tags);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Add a new tag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $params = $request->only('name');

        $rules = array(
            'name' => 'required|min:2',
        );

        // In case of error it will throw an exception and will be handled with Json error response
        $this->validate($request, $rules);

        $this->tagRepository->insert($params);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Update an existing tag by tag id
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $params = $request->only('name');

        $rules = array(
            'name' => 'required|min:2',
        );

        // In case of error it will throw an exception and will be handled with Json error response
        $this->validate($request, $rules);

        $this->tagRepository->update($id, $params);

        return Response::json($this->response['data'], $this->response['code']);
    }

    /**
     * Delete an existing tag by tag id
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // In case of error it will throw an exception and will be handled with Json error response
        $this->tagRepository->delete($id);

        return Response::json($this->response['data'], $this->response['code']);
    }
}
