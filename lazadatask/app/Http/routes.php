<?php

Route::get('/', function () {
    return View::make('index');
});


Route::get('/swagger/json', function () {
    return Response::make(File::get(storage_path('app/api/swagger.json')))->header('Content-Type', 'application/json');
});

Route::group(['prefix' => 'api/v1', 'namespace' => 'Api'], function () {
    Route::get('posts/count', 'PostController@count');
    Route::post('posts/{id}/tags/{tagId}', 'PostController@addTag');
    Route::delete('posts/{id}/tags/{tagId}', 'PostController@deleteTag');

    // Restful CRUD access for posts
    Route::resource('posts', 'PostController');

    // Restful CRUD access for tags
    Route::resource('tags', 'TagController');
});
