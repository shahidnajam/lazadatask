<?php
namespace App\Lazada\Repositories\Post;

use App\Post;
use App\Events\PostDeleted;
use App\Lazada\Services\Cache\CacheInterface;

/**
 * Class PostRepository
 * @package App\Lazada\Repositories
 */
class PostRepository implements PostRepositoryInterface
{
    /**
     * @var Post
     */
    protected $model;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * Inject dependencies
     *
     * @param Post $post
     * @param CacheInterface $cache
     */
    public function __construct(Post $post, CacheInterface $cache)
    {
        $this->model = $post;
        $this->cache = $cache;
    }

    /**
     * Get all posts
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $key = md5('all');

        $posts = $this->cache->get($key);

        if (is_null($posts)) {
            $posts = $this->model->with('tags')->get();
            $this->cache->put($key, $posts);
        }

        return $posts;
    }

    /**
     * Get all posts by tags
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByTags(array $tags)
    {
        $key = md5('byTag.' . serialize($tags));

        $posts = $this->cache->get($key);

        if (is_null($posts)) {
            $posts = $this->model->with('tags')->whereHas('tags', function ($query) use ($tags) {
                $query->whereIn('name', $tags);
            })->get();

            $this->cache->put($key, $posts);
        }

        return $posts;
    }

    /**
     * Count all posts
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function countAll()
    {
        // Get the count from getAll() data as it is cached.
        $posts = $this->getAll();

        return $posts->count();
    }

    /**
     * Count all posts by tag or tags
     *
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function countByTags(array $tags)
    {
        // Get the count from getByTags() data as it is cached.
        $posts = $this->getByTags($tags);

        return $posts->count();
    }

    /**
     * Insert a new post
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        $result = $this->model->create($data);
        // Flush cache only for posts related cache
        $this->cache->flush();
        return $result;
    }

    /**
     * Update post by an id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $result = $this->model->findOrFail($id)->update($data);
        // Flush cache only for posts related cache
        $this->cache->flush();
        return $result;
    }

    /**
     * Delete post by an id
     *
     * @param  int $id
     * @return mixed
     */
    public function delete($id)
    {
        $model = $this->model->findOrFail($id);
        $result = $model->delete($id);

        // Event on post deleting
        \Event::fire(new PostDeleted($model));

        // Flush cache only for posts related cache
        $this->cache->flush();

        return $result;
    }

    /**
     * Add tag to post
     *
     * @param int $id
     * @param int $tagId
     * @return mixed
     */
    public function addTag($id, $tagId)
    {
        $post = $this->model->findOrFail($id);

        if (!$post->tags->contains('id', $tagId)) {
            $result = $post->tags()->attach($tagId);
            // Flush cache only for posts related cache
            $this->cache->flush();
            return $result;
        }

        return false;
    }

    /**
     * Delete or remove the tag from the post
     *
     * @param int $id
     * @param int $tagId
     * @return mixed
     */
    public function deleteTag($id, $tagId)
    {
        $result = $this->model->findOrFail($id)->tags()->detach($tagId);
        // Flush cache only for posts tag
        $this->cache->flush();
        return $result;
    }
}
