<?php

namespace App\Lazada\Repositories\Tag;

use App\Tag;
use App\Lazada\Services\Cache\CacheInterface;

/**
 * Class TagRepository
 * @package App\Lazada\Repositories
 */
class TagRepository implements TagRepositoryInterface
{
    /**
     * @var Tag
     */
    protected $model;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * Inject dependencies
     *
     * @param Tag $tag
     * @param CacheInterface $cache
     */
    public function __construct(Tag $tag, CacheInterface $cache)
    {
        $this->model = $tag;
        $this->cache = $cache;
    }

    /**
     * Find and get tag by an id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function find($id)
    {
        $key = md5('id.' . $id);
        $tag = $this->cache->get($key);

        if (is_null($tag)) {
            $tag = $this->model->findOrFail($id);
            $this->cache->put($key, $tag);
        }

        return $tag;
    }

    /**
     * Get all tags
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $key = md5('all');
        $tags = $this->cache->get($key);

        if (is_null($tags)) {
            $tags = $this->model->get();
            $this->cache->put($key, $tags);
        }

        return $tags;
    }

    /**
     * Insert a new post
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        $result = $this->model->create($data);
        // Flush cache both for tags and posts as it can affect post result
        $this->cache->flush();
        return $result;
    }

    /**
     * Update post by an id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $result = $this->model->findOrFail($id)->update($data);
        // Flush cache both for tags and posts as it can affect post result
        $this->cache->flush();
        return $result;
    }

    /**
     * Delete post by an id
     *
     * @param  int $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->model->findOrFail($id)->delete($id);
        // Flush cache both for tags and posts as it can affect post result
        $this->cache->flush();
        return $result;
    }
}
