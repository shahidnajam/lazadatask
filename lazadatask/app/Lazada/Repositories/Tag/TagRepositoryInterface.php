<?php

namespace App\Lazada\Repositories\Tag;

/**
 * Interface TagRepositoryInterface
 * @package App\Lazada\Repositories\Tag
 */
interface TagRepositoryInterface
{

    /**
     * Find and get tag by an id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function find($id);

    /**
     * Get all tags
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll();

    /**
     * Insert a new tag
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data);

    /**
     * Update tag by an id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * Delete tag by an id
     *
     * @param  int $id
     * @return mixed
     */
    public function delete($id);
}