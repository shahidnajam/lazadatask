<?php

namespace App\Lazada\Services\Cache;

use Illuminate\Cache\CacheManager;

/**
 * Class LaravelCache
 * @package App\Lazada\Services\Cache
 */
class LaravelCache implements CacheInterface
{
    /**
     * @var \Illuminate\Cache\CacheManager
     */
    protected $cache;

    /**
     * @var string
     */
    protected $tags;

    /**
     * @var integer
     */
    protected $minutes;

    /**
     * Construct
     *
     * @param \Illuminate\Cache\CacheManager $cache
     * @param string $tag
     * @param integer $minutes
     */
    public function __construct(CacheManager $cache, $tag, $minutes = 60)
    {
        $this->cache = $cache;
        $this->tags = $tag;
        $this->minutes = $minutes;
    }

    /**
     * Get cache with key
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->cache->tags($this->tags)->get($key);
    }

    /**
     * Put/Store cache
     *
     * @param string $key
     * @param mixed $value
     * @param integer $minutes
     * @return mixed
     */
    public function put($key, $value, $minutes = null)
    {
        if (is_null($minutes)) {
            $minutes = $this->minutes;
        }

        return $this->cache->tags($this->tags)->put($key, $value, $minutes);
    }

    /**
     * Flush cache for tags.
     *
     * @param  mixed $tags
     *
     * @return bool
     */
    public function flush()
    {
        return $this->cache->tags($this->tags)->flush();
    }
}