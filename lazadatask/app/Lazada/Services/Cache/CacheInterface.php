<?php

namespace App\Lazada\Services\Cache;

/**
 * Interface CacheInterface
 * @package App\Lazada\Services\Cache
 */
interface CacheInterface
{
    /**
     * Get cache
     *
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * Put cache
     *
     * @param string $key
     * @param mixed $value
     * @param integer $minutes
     * @return mixed
     */
    public function put($key, $value, $minutes = null);

    /**
     * Flush cache for tags.
     *
     * @param  mixed $tags
     *
     * @return bool
     */
    public function flush();
}