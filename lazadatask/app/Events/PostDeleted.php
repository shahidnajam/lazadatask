<?php

namespace App\Events;

use App\Post;

/**
 * Class PostDeleted
 * @package App\Events
 */
class PostDeleted extends Event
{
    /**
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Post $data)
    {
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
