<?php

use Mockery as m;
use App\Lazada\Repositories\Tag\TagRepository;

/**
 * Class TagRepositoryTest
 */
class TagRepositoryTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $model;

    /**
     * @var \Mockery\MockInterface
     */
    protected $cache;
    /**
     * @var \App\Lazada\Repositories\Tag\TagRepositoryInterface
     */
    protected $repository;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->cache = m::mock(\App\Lazada\Services\Cache\LaravelCache::class);
        $this->model = m::mock(\App\Tag::class);

        $this->repository = new TagRepository($this->model, $this->cache);
    }

    /**
     *
     */
    public function tearDown()
    {
        m::close();
    }

    /**
     * Check the find() method when data is not cached
     */
    public function testFindDb()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $tag = [
            'id' => 4,
            'name' => 'php',
            'updated_at' => '2016-08-12 10:16:17',
        ];

        $this->model->shouldReceive('findOrFail')->once()->with(4)->AndReturn($tag);

        $this->assertEquals($tag, $this->repository->find(4));
    }

    /**
     * Check the find() method when data is already cached
     */
    public function testFindCached()
    {
        $cachedTag = [
            'id' => 4,
            'name' => 'php',
            'updated_at' => '2016-08-12 10:16:17',
        ];

        $this->cache->shouldReceive('get')->once()->AndReturn($cachedTag);

        $this->model->shouldNotReceive('findOrFail');

        $this->assertEquals($cachedTag, $this->repository->find(4));
    }

    /**
     * Check the getAll() method when data is not cached
     */
    public function testGetAllDb()
    {
        $this->cache->shouldReceive('get')->once()->AndReturnNull();
        $this->cache->shouldReceive('put')->once();

        $tag = [
            'id' => 4,
            'name' => 'php',
            'updated_at' => '2016-08-12 10:16:17',
        ];

        $this->model->shouldReceive('get')->once()->AndReturn($tag);

        $this->assertEquals($tag, $this->repository->getAll());
    }

    /**
     * Check the getAll() method when data is already cached
     */
    public function testGetAllCached()
    {
        $cachedTag = [
            'id' => 4,
            'name' => 'php',
            'updated_at' => '2016-08-12 10:16:17',
        ];

        $this->cache->shouldReceive('get')->once()->AndReturn($cachedTag);

        $this->model->shouldNotReceive('get');

        $this->assertEquals($cachedTag, $this->repository->getAll());
    }

    /**
     * Test insert()
     */
    public function testInsert()
    {
        $inputData = ['name' => 'php'];

        $this->model->shouldReceive('create')->once()->with($inputData)->andReturn(true);
        $this->cache->shouldReceive('flush')->once();

        $this->assertTrue($this->repository->insert($inputData));
    }

    /**
     * Test update()
     */
    public function testUpdate()
    {
        $inputId = 1;
        $inputData = array('name' => 'php');

        $this->model->shouldReceive('findOrFail')->once()->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('update')->once()->with($inputData)->andReturn(true);
        $this->cache->shouldReceive('flush')->once();

        $this->repository->update($inputId, $inputData);
    }

    /**
     * Test delete()
     */
    public function testDelete()
    {
        $inputId = 3;

        $this->model->shouldReceive('findOrFail')->once()->with($inputId)->AndReturn(m::self());
        $this->model->shouldReceive('delete')->once()->andReturn(true);
        $this->cache->shouldReceive('flush')->once();

        $this->repository->delete($inputId);
    }
}
