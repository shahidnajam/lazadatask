<?php

use Mockery as m;
use App\Lazada\Repositories\Post\PostRepositoryInterface;
use App\Lazada\Repositories\Tag\TagRepositoryInterface;

/**
 * Class PostRepositoryTest
 */
class PostControllerTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $model;

    /**
     * @var \Mockery\MockInterface
     */
    protected $cache;

    /**
     * @var \App\Lazada\Repositories\Post\PostRepositoryInterface
     */
    protected $repository;

    /**
     * @var \Mockery\MockInterface
     */
    protected $mockedPostRepo;

    /**
     * @var \Mockery\MockInterface
     */
    protected $mockedTagRepo;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->mockedPostRepo = Mockery::mock(PostRepositoryInterface::class);
        $this->mockedTagRepo = Mockery::mock(TagRepositoryInterface::class);

        $this->app->instance(TagRepositoryInterface::class, $this->mockedTagRepo);
        $this->app->instance(PostRepositoryInterface::class, $this->mockedPostRepo);
    }

    /**
     *
     */
    public function tearDown()
    {
        m::close();
    }

    /**
     * Get all post without tag param passed
     */
    public function testIndexWithoutTags()
    {
        $data = [
            'title' => 'Test title',
            'author' => 'Test author',
            'body' => 'This is a test body'
        ];

        $this->mockedPostRepo->shouldReceive('getAll')->once()->AndReturn($data);

        $this->call('GET', 'api/v1/posts');

        // Check response is success and posts exists in json
        $this->assertResponseOk();
        $this->seeJsonContains(['posts' => $data]);
    }

    /**
     * Get all post without tag param passed
     */
    public function testIndexWithTags()
    {
        $data = [
            'title' => 'Test title',
            'author' => 'Test author',
            'body' => 'This is a test body',
            'tags' => ['java']
        ];

        $this->mockedPostRepo->shouldReceive('getByTags')->once()->AndReturn($data);

        $this->call('GET', 'api/v1/posts', ['tags' => 'java']);

        // Check response is success and posts exists in json
        $this->assertResponseOk();
        $this->seeJsonContains(['posts' => $data]);
    }


    /**
     * Count all post without tag param passed
     */
    public function testCountWithoutTags()
    {
        $count = 4;

        $this->mockedPostRepo->shouldReceive('countAll')->once()->AndReturn($count);

        $this->call('GET', 'api/v1/posts/count');

        // Check response is success and posts exists in json
        $this->assertResponseOk();
        $this->seeJsonContains(['total_posts' => $count]);
    }

    /**
     * Count all post without tag param passed
     */
    public function testCountWithTags()
    {
        $count = 4;

        $this->mockedPostRepo->shouldReceive('countByTags')->once()->AndReturn($count);

        $this->call('GET', 'api/v1/posts/count', ['tags' => 'java']);

        // Check response is success and posts exists in json
        $this->assertResponseOk();
        $this->seeJsonContains(['total_posts' => $count]);
    }

    /**
     *
     */
    public function testStoreSuccess()
    {
        $data = [
            'title' => 'Test title',
            'author' => 'Test author',
            'body' => 'This is a test body'
        ];

        $this->mockedPostRepo->shouldReceive('insert')->once();
        \Event::shouldReceive('fire');

        $this->call('POST', 'api/v1/posts', $data);

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testStoreFails()
    {
        $data = ['title' => 'Tee', 'author' => 'Tes'];

        $this->mockedPostRepo->shouldReceive('insert')->never();

        $this->call('POST', 'api/v1/posts', $data);

        $this->assertResponseStatus(422);
    }

    /**
     *
     */
    public function testUpdateSuccess()
    {
        $data = [
            'title' => 'Test title',
            'author' => 'Test author',
            'body' => 'This is a test body'
        ];

        $this->mockedPostRepo->shouldReceive('update')->once();

        $this->call('PUT', 'api/v1/posts/2', $data);

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testUpdateFails()
    {
        $data = ['title' => 'Tee', 'author' => 'Tes'];

        $this->mockedPostRepo->shouldReceive('update')->never();

        $this->call('PUT', 'api/v1/posts/2', $data);

        $this->assertResponseStatus(422);
    }

    /**
     *
     */
    public function testDelete()
    {
        $this->mockedPostRepo->shouldReceive('delete')->with(2)->once()->andReturn(null);

        $this->call('DELETE', 'api/v1/posts/2');

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testAddTag()
    {
        $this->mockedTagRepo->shouldReceive('find')->once();
        $this->mockedPostRepo->shouldReceive('addTag')->once();

        $this->call('POST', 'api/v1/posts/2/tags/1');

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testDeleteTag()
    {
        $this->mockedTagRepo->shouldReceive('find')->once();
        $this->mockedPostRepo->shouldReceive('deleteTag')->once();

        $this->call('DELETE', 'api/v1/posts/2/tags/1');

        $this->assertResponseOk();
    }
}
