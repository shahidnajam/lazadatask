<?php

use Mockery as m;
use App\Lazada\Repositories\Tag\TagRepositoryInterface;

/**
 * Class TagRepositoryTest
 */
class TagControllerTest extends TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $model;

    /**
     * @var \Mockery\MockInterface
     */
    protected $cache;

    /**
     * @var \App\Lazada\Repositories\Tag\TagRepositoryInterface
     */
    protected $repository;

    /**
     * @var \Mockery\MockInterface
     */
    protected $mockedTagRepo;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->mockedTagRepo = Mockery::mock(TagRepositoryInterface::class);

        $this->app->instance(TagRepositoryInterface::class, $this->mockedTagRepo);
    }

    /**
     *
     */
    public function tearDown()
    {
        m::close();
    }

    /**
     *
     */
    public function testIndex()
    {
        $data = ['name' => 'java'];
        $this->mockedTagRepo->shouldReceive('getAll')->once()->AndReturn($data);

        $this->call('GET', 'api/v1/tags');

        // Check response is success and tags exists in json
        $this->assertResponseOk();
        $this->seeJsonContains(['tags' => $data]);
    }

    /**
     *
     */
    public function testStoreSuccess()
    {
        $data = ['name' => 'java'];

        $this->mockedTagRepo->shouldReceive('insert')->once();

        $this->call('POST', 'api/v1/tags', $data);

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testStoreFails()
    {
        $data = ['name' => null];

        $this->mockedTagRepo->shouldReceive('insert')->never();

        $this->call('POST', 'api/v1/tags', $data);

        $this->assertResponseStatus(422);
    }

    /**
     *
     */
    public function testUpdateSuccess()
    {
        $data = ['name' => 'java'];

        $this->mockedTagRepo->shouldReceive('update')->once();

        $this->call('PUT', 'api/v1/tags/2', $data);

        $this->assertResponseOk();
    }

    /**
     *
     */
    public function testUpdateFails()
    {
        $data = ['name' => null];

        $this->mockedTagRepo->shouldReceive('update')->never();

        $this->call('PUT', 'api/v1/tags/2', $data);

        $this->assertResponseStatus(422);
    }

    /**
     *
     */
    public function testDelete()
    {
        $this->mockedTagRepo->shouldReceive('delete')->with(2)->once()->andReturn(null);

        $this->call('DELETE', 'api/v1/tags/2');

        $this->assertResponseOk();
    }
}
