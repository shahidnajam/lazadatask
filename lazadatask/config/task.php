<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Email setting
    |--------------------------------------------------------------------------
    |
    */

    'mail' => [
        'from' => ['address' => 'test@lazada.com', 'name' => 'Shahid'],
        'to' => ['address' => 'shahidnjafridi@outlook.com', 'name' => null]
    ]
];
